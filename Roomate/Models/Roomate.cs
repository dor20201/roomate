﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roomate.Models
{
    public class Roomate
    {
        #region properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string CellPhone { get; set; }
        public string Mail { get; set; }
        public Religion Religion { get; set; }
        public Nutrition Nutrition { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public int Budget { get; set; }
        public Area Area { get; set; }
        public string Disorder { get; set; }
        public string Description { get; set; }
        public bool IsSmokimgAllowed { get; set; }
        public bool IsAnimalFriendly { get; set; }
        public FamilyStatus FamilyStatus { get; set; }
        public bool IsActive { get; set; }
        public string PictureUrl { get; set; }
        public int Rank { get; set; }
        #endregion
    }
}
