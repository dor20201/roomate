﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roomate.Models
{
    public class Request
    {
        #region properties
        public int Id { get; set; }
        public User From { get; set; }
        public User To { get; set; }
        public RequestStatus Status { get; set; }
        public string Message { get; set; }
        public int CommonFriends { get; set; }
        #endregion
    }
}
