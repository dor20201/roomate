﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roomate.Models
{
    public class Area
    {
        public int Id { get; set; }
        public int Name { get; set; }
    }
}
