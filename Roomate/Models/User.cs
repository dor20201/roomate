﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roomate.Models
{
    public class User
    {
        #region properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string CellPhone { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }
        public UserType Type { get; set; }
        public bool IsBlocked { get; set; }
        public string FacebookUrl { get; set; }
        #endregion
    }
}
