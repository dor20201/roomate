﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roomate.Models
{
    public class FamilyStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
